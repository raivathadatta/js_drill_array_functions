import ErrorStatement from '../errorstatements.js'

export default function find(array, cd) {
    try {
        if (Array.isArray(array)) {
            if (typeof cd == 'function') {
                for (let index = 0; index < array.length; index++) {
                    let element = cd(array[index], index, array)
                    if (element) {
                        return array[index]
                    }
                }
                return undefined
            }
            else {
                throw ErrorStatement.notAFunction
            }
        } else {
            throw ErrorStatement.notAnArray
        }
    } catch (error) {
        console.log(error)
    }
}