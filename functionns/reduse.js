
import ErrorStatement from '../errorstatements.js'

export default function reduce(array, cd,initial=array[0]) {
 try{
    if (Array.isArray(array)) {
        if (typeof cd == 'function') {
            for (let index = 0; index < array.length; index++) {
                initial = cd(initial, array[index], index, array)
            }
            return initial
        }
        else {
            throw ErrorStatement.notAFunction
        }
    }
    else {
        throw ErrorStatement.notAnArray
    }
 }catch(error){
    console.log(error);
 }
}