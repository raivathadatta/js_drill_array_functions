
import ErrorStatement from '../errorstatements.js'

export default function map(array, cd) {
 try {
    if (Array.isArray(array)) {
        if (typeof cd == 'function') {

            let outcomeArray = []
            let index = 0
            
            while (index < array.length) {
                outcomeArray.push(cd(array[index], index, array))
                index++
            }
        
            return outcomeArray
        }
        else {
            throw ErrorStatement.notAFunction
        }
    } 
    else {
        throw ErrorStatement.notAnArray
    }
 } catch (error) {
    console.log(error)
 }
}