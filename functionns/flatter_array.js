import ErrorStatement from '../errorstatements.js'

export default function flatterArray(array, cd) {
    try {
        if (Array.isArray(array)) {
            
            let tempArray = []

            for (let index = 0; index < array.length; index++)
                if (Array.isArray(array[index])) {
                    tempArray = tempArray.concat(flatterArray(array[index]))
                }
                else {
                    tempArray.push(array[index])
                }

            return tempArray

        } else {
            throw ErrorStatement.notAnArray
        }
    } catch (error) {
        console.log(error)
    }
}