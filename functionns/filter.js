import ErrorStatement from '../errorstatements.js'

function filter(array, cd) {
  try{
    if (Array.isArray(array)) {
        if (typeof cd == 'function') {
            var filteredArray = []            
            for (let index = 0; index < array.length; index++) {
                let outcome = cd(array[index], index, array)
                if (outcome) {
                    filteredArray.push(array[index])
                }
            }
            return filteredArray
        } 
        else {
            throw ErrorStatement.notAFunction
        }
    } else {
        throw ErrorStatement.notAnArray
    }
  }catch(error){
    console.log(error)
  }
}

export default filter 