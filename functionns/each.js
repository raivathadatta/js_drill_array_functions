
import ErrorStatement from '../errorstatements.js'

export default function each(array, cd) {
    try {
        if (Array.isArray(array)) {
            if (typeof cd == 'function') {
                for (let index = 0; index < array.length; index++) {
                    cd(array[index], index, array)
                }
            }
            else {
                throw ErrorStatement.notAFunction
            }
        } else {

            throw ErrorStatement.notAnArray
        }
    } catch (error) {
        console.log(error)
    }
}